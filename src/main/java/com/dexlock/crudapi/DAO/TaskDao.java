package com.dexlock.crudapi.DAO;

import com.dexlock.crudapi.Model.Task;
import com.google.gson.Gson;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;


public class TaskDao {
    Gson gson= new Gson();
    public Document addTask(MongoCollection<Document> collectionTask,MongoCollection<Document> collectionUser,MongoCollection<Document> collectionAccessToken, Task task){
//        Document d = collectionAccessToken.find({"Email" : 1});
        Document newTask=Document.parse(gson.toJson(task));
        collectionTask.insertOne(newTask);
        return newTask;

    }

    public List<Document> viewTaskDetails(MongoCollection<Document> collectionTask){
        List<Document> tasks = collectionTask.find().into(new ArrayList<Document>());
        return tasks;

    }
}
