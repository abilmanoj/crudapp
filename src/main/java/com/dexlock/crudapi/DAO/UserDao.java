package com.dexlock.crudapi.DAO;

import com.dexlock.crudapi.Model.User;
import com.google.gson.Gson;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    Gson gson=new Gson();



    public Document addUser(MongoCollection<Document> collectionUser, User userData) {
        Document user = Document.parse(gson.toJson(userData));
        collectionUser.insertOne(user);
        return user;
    }
    public List<Document> viewUser(MongoCollection<Document> collectionUser){
        List<Document> user = collectionUser.find().into(new ArrayList<Document>());
        return user;

    }
    public List<Document> userLogin(MongoCollection<Document> collectionUser,String email,String password)
    {
        List<Document> user = collectionUser.find((new Document("userEmail",email)).append("password",password)).into(new ArrayList<Document>());
        return  user;
    }
    public Response addAccessToken(MongoCollection<Document> collectionAccessToken, String email, String accessToken){
        collectionAccessToken.insertOne(new Document("AccessToken",accessToken).append("Email",email));
        return Response.ok().build();

    }

    public List<Document> tokenDelete(MongoCollection<Document> collectionAccessToken,String paramVal){
        collectionAccessToken.deleteOne(new Document("AccessToken",paramVal));
        List<Document> tokens_s = collectionAccessToken.find().into(new ArrayList<Document>());
        return tokens_s;
    }
}

