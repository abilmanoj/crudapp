package com.dexlock.crudapi.Application.Resource;

import com.dexlock.crudapi.DAO.UserDao;
import com.dexlock.crudapi.Model.User;
import com.google.gson.Gson;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;


@Path("/user")
 public class UserController {
   private MongoCollection<Document> collectionAccessToken;
    private MongoCollection<Document> collectionUser;
    UserDao daoUser = new UserDao();

    public UserController(MongoCollection<Document> collectionUser,MongoCollection<Document> collectionAccessToken) {
        this.collectionUser = collectionUser;
        this.collectionAccessToken=collectionAccessToken;
    }

    @Path("/insert")
    @POST
    @Produces(MediaType.APPLICATION_JSON)

    public Response insertUser(User userData) {
        Document addUserData = daoUser.addUser(collectionUser, userData);
        return Response.ok(addUserData).build();
    }

    @Path("/view")
    @GET

    @Produces(MediaType.APPLICATION_JSON)
    public Response viewAllUser() {
        List<Document> viewUser = daoUser.viewUser(collectionUser);
        return Response.ok(viewUser).build();

    }

    @Path("/login")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response userLogin(@QueryParam("userEmail") String email, @QueryParam("password") String password) {
           String obj="null";

           List<Document> document=daoUser.userLogin(collectionUser,email, password);
           if (document.size()<1) {
                obj="user not found";
           }
           else {
               String accessToken= UUID.randomUUID().toString();
               daoUser.addAccessToken(collectionAccessToken,email,accessToken);


               obj = "user found";

           }
        return Response.ok(obj).build();
    }

    @Path("/deletetoken")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteToken(@QueryParam("AccessToken") String paramVal){
        List<Document> document=daoUser.tokenDelete(collectionAccessToken,paramVal);
        return Response.ok(document).build();
    }

}
