package com.dexlock.crudapi.Application.Resource;

import com.dexlock.crudapi.DAO.TaskDao;
import com.dexlock.crudapi.Model.Task;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/task")
public class TaskController {
    TaskDao daoTask=new TaskDao();
    private  MongoCollection<Document> collectionTask;
    private  MongoCollection<Document> collectionUser;
    private  MongoCollection<Document> collectionAccessToken;

    public TaskController(MongoCollection<Document> collectionTask,MongoCollection<Document> collectionUser,MongoCollection<Document> collectionAccessToken) {
        this.collectionTask=collectionTask;
        this.collectionUser=collectionUser;
        this.collectionAccessToken=collectionAccessToken;

    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addTask(Task task){
        Document document = daoTask.addTask(collectionTask,collectionUser,collectionAccessToken,task) ;
        return Response.ok(document).build();

    }

    @Path("/viewtask")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response viewTask(){
        List<Document> viewTasks = daoTask.viewTaskDetails(collectionTask);
        return Response.ok(viewTasks).build();
    }
}

