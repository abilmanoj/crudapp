package com.dexlock.crudapi.Application;

import io.dropwizard.Configuration;

public class ApplicationConfig extends Configuration
 {
     private String mongoHost;
     private Integer mongoPort;
     private String mongoDB;


     public String getMongoHost() {
         return mongoHost;
     }

     public Integer getMongoPort() {
         return mongoPort;
     }

     public String getMongoDB() {
         return mongoDB;
     }
 }
