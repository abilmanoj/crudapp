package com.dexlock.crudapi.Application;

import com.dexlock.crudapi.Application.Resource.CommentController;
import com.dexlock.crudapi.Application.Resource.TaskController;
import com.dexlock.crudapi.Application.Resource.UserController;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.bson.Document;

public class CRUDApplication extends Application<ApplicationConfig> {
    public void run(ApplicationConfig applicationConfig, Environment environment) throws Exception {

        MongoClient mongo = new MongoClient(applicationConfig.getMongoHost(), applicationConfig.getMongoPort());
        MongoDatabase database = mongo.getDatabase(applicationConfig.getMongoDB());
        MongoCollection<Document> collectionUser= database.getCollection("User");
        MongoCollection<Document> collectionTask= database.getCollection("Task");
        MongoCollection<Document> collectionComment= database.getCollection("Comment");
        MongoCollection<Document> collectionAccessToken= database.getCollection("AccessToken");
        environment.jersey().register( new UserController(collectionUser,collectionAccessToken));
        environment.jersey().register( new TaskController(collectionTask,collectionUser,collectionAccessToken));
        environment.jersey().register( new CommentController(collectionComment));




    }

    @Override
    public String getName() {
        return "Task Manager";
    }

    @Override
    public void initialize(Bootstrap<ApplicationConfig> bootstrap) {
        super.initialize(bootstrap);
    }

    public static void main(String[] args) throws Exception {
        new CRUDApplication().run("server","config.yml");
    }
}
