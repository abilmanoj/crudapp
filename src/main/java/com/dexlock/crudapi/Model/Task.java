package com.dexlock.crudapi.Model;

import java.awt.*;
import java.util.ArrayList;
import java.util.UUID;

public class Task {
    private UUID taskNumber;
    private String taskTitle;
    public enum taskType{Features,Subtask,Enhancement,Bug,Task}
    private Task.taskType typeObj;
    private String taskDescription;
    private String taskAcceptanceCriteria;
    private String taskStatus;
    private String taskDate;
    private String taskDueDate;
    private float  taskEstimate;
    private String taskReporter;
   // private ArrayList<String> commentList = new ArrayList<String>();


    public void setTaskNumber(UUID taskNumber) {
        this.taskNumber = UUID.randomUUID();
    }

    public Task(UUID taskNumber, String taskTitle, Task.taskType typeObj, String taskDescription,
                String taskAcceptanceCriteria, String taskStatus, String taskDate, String taskDueDate
            , float taskEstimate, String taskReporter) {
        this.taskNumber = taskNumber;
        this.taskTitle = taskTitle;
        this.typeObj = typeObj;
        this.taskDescription = taskDescription;
        this.taskAcceptanceCriteria = taskAcceptanceCriteria;
        this.taskStatus = taskStatus;
        this.taskDate = taskDate;
        this.taskDueDate = taskDueDate;
        this.taskEstimate = taskEstimate;
        this.taskReporter = taskReporter;
    }
    public Task(){

    }

    public UUID getTaskNumber() {
        return taskNumber;
    }



    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public Task.taskType getTypeObj() {
        return typeObj;
    }

    public void setTypeObj(Task.taskType typeObj) {
        this.typeObj = typeObj;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskAcceptanceCriteria() {
        return taskAcceptanceCriteria;
    }

    public void setTaskAcceptanceCriteria(String taskAcceptanceCriteria) {
        this.taskAcceptanceCriteria = taskAcceptanceCriteria;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }

    public String getTaskDueDate() {
        return taskDueDate;
    }

    public void setTaskDueDate(String taskDueDate) {
        this.taskDueDate = taskDueDate;
    }

    public float getTaskEstimate() {
        return taskEstimate;
    }

    public void setTaskEstimate(float taskEstimate) {
        this.taskEstimate = taskEstimate;
    }

    public String getTaskReporter() {
        return taskReporter;
    }

    public void setTaskReporter(String taskReporter) {
        this.taskReporter = taskReporter;
    }
}
