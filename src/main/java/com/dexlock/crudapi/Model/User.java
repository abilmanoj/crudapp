package com.dexlock.crudapi.Model;

import java.util.UUID;

public class User {
    private UUID userId;
    private String userName;
    private String userEmail;
    public enum userRole{SuperAdmin,ProjectManager,User}
    private userRole roleObj;
    private String password;

    public User(UUID userId, String userName, String userEmail, userRole roleObj, String password) {
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.roleObj = roleObj;
        this.password = password;
    }
    public User(){

    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = UUID.randomUUID();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public userRole getRoleObj() {
        return roleObj;
    }

    public void setRoleObj(userRole roleObj) {
        this.roleObj = roleObj;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
