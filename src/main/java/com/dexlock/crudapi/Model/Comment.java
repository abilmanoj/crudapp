package com.dexlock.crudapi.Model;

import java.util.UUID;

public class Comment {
    private UUID commentId;
    private String CommentUserId;
    private String message;
    private String LastUpdatedTime;

    public Comment(UUID commentId, String commentUserId, String message, String lastUpdatedTime) {
        this.commentId = commentId;
        CommentUserId = commentUserId;
        this.message = message;
        LastUpdatedTime = lastUpdatedTime;
    }
    public Comment(){

    }

    public UUID getCommentId() {
        return commentId;
    }

    public void setCommentId(UUID commentId) {
        this.commentId = commentId;
    }

    public String getCommentUserId() {
        return CommentUserId;
    }

    public void setCommentUserId(String commentUserId) {
        CommentUserId = commentUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLastUpdatedTime() {
        return LastUpdatedTime;
    }

    public void setLastUpdatedTime(String lastUpdatedTime) {
        LastUpdatedTime = lastUpdatedTime;
    }
}
